package test;

import static org.junit.Assert.*;

import org.junit.Test;

import pokertexas.model.Carta;
import pokertexas.model.Dealer;
import pokertexas.model.Game;

public class TestGame {

	@Test 
	public void testConstruccionDeJuego() {
		int numJugadores=5;
		String nombreJugador="Daly";
		double montoInicial=1000000;		
		Game game = new Game(nombreJugador,numJugadores,montoInicial);
		assertEquals(nombreJugador, game.getNombreJugador());
		assertEquals(numJugadores, game.getNumJugadores());
		assertEquals(montoInicial, game.getMontoInicial(),.01);
	}


	@Test 
	public void testCrearJugador() {
		Game game  =  new Game("Juan", 3, 5000);
		assertEquals("Juan", game.getJugador().getNombre());
		assertEquals(5000, game.getJugador().getEfectivo(),.01);
	}

	@Test 
	public void testCrearJugadoresDeLaMesa() {
		Game game  =  new Game("Juan", 3, 5000);
		assertEquals("Juan", game.getJugador().getNombre());
		assertEquals(2, game.getNumeroContrincantes());
	}


	@Test 
	public void testCrearDealer() {
		Game game  =  new Game("Juan", 3, 5000);
		assertNotNull(game.getDealer());
	}
	
	
	@Test 
	public void testDealerReparteCarta() {
		Game game  =  new Game("Juan", 3, 5000);
		assertNotNull(game.getDealer());
		Dealer dealer = game.getDealer();
		assertEquals(52,dealer.getMazo().size());
		Carta carta = dealer.sacaCarta();
		
	}
	



}
