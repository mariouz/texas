package pokertexas.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class MenuInicial extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuInicial frame = new MenuInicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuInicial() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Poker Texas");
		setBounds(100, 100, 450, 433);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		agregarBotones();



	}

	private void agregarBotones() {
		GridLayout layout = new GridLayout(5,1);
		layout.setVgap(10);
		JPanel panelCentral = new JPanel(layout);
		panelCentral.setBorder(new EmptyBorder(10, 60, 30, 60));
		JButton buttonPartidaRapida = new JButton("Partida R�pida");
		buttonPartidaRapida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goToGame();
			}
		});

		JLabel icono = new JLabel("");
		icono.setHorizontalAlignment(SwingConstants.CENTER);
		icono.setIcon(new ImageIcon("C:\\Users\\Mario\\Documents\\Proyectos\\COMUNIDAD DE DESARROLLADORES\\logo alone-02.png"));
		panelCentral.add(icono);


		JButton buttonTorneo= new JButton("Torneo");
		JButton buttonOpciones = new JButton("Partida Opciones");
		JButton buttonMarcadores= new JButton("Marcadores");
		panelCentral.add(buttonPartidaRapida, BorderLayout.CENTER);
		panelCentral.add(buttonTorneo, BorderLayout.CENTER);
		panelCentral.add(buttonOpciones, BorderLayout.CENTER);
		panelCentral.add(buttonMarcadores, BorderLayout.CENTER);
		contentPane.add(panelCentral);
	}
	private void goToGame() {
		// TODO Auto-generated method stub

	}

}
