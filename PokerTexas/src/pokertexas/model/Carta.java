package pokertexas.model;

public class Carta {
	private int valor;
	public Carta(int valor, TipoCarta tipo) {
		super();
		this.valor = valor;
		this.tipo = tipo;
	}
	private TipoCarta tipo;
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public TipoCarta getTipo() {
		return tipo;
	}
	public void setTipo(TipoCarta tipo) {
		this.tipo = tipo;
	}

}
