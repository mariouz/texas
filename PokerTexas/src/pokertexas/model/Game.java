package pokertexas.model;

import java.util.ArrayList;

public class Game {

	private String nombreJugador;
	private int numJugadores;
	private double montoInicial;
	Jugador jugador;
	Dealer dealer;
	ArrayList<Jugador> listaJugadores =new ArrayList<Jugador>();

	public Game(String nombreJugador, int numJugadores, double montoInicial) {
		this.nombreJugador = nombreJugador;
		this.numJugadores = numJugadores;
		this.montoInicial=montoInicial;
		jugador= new Jugador(nombreJugador, montoInicial);
		generaContrincantes (getNumeroContrincantes());
		listaJugadores.add(jugador);
		dealer= new Dealer();
	}

	private void generaContrincantes(int numeroContrincantes) {
		for (int i = 0; i < numeroContrincantes; i++) {
			Jugador jugador = new Jugador("Pancleto"+i, montoInicial);
			listaJugadores.add(jugador);
		}


	}

	public String getNombreJugador() {
		return nombreJugador;
	}

	public int getNumJugadores() {
		return numJugadores;
	}

	public double getMontoInicial() {
		return montoInicial;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public int getNumeroContrincantes() {
		return numJugadores-1;
	}

	public Dealer getDealer() {
		return dealer;
	}

}
