package pokertexas.model;

import java.util.ArrayList;
import java.util.Random;

public class Dealer {

	ArrayList<Carta> mazo =  new ArrayList<Carta>();
	public Dealer (){
		crearMazoCartas();
	}
	public ArrayList<Carta> getMazo() {
		return mazo;
	}
	
	private void crearMazoCartas() {
		TipoCarta[] tiposCartas ={TipoCarta.CORAZON,TipoCarta.PICA,TipoCarta.ESPADA, TipoCarta.TREBOL};
		for (int i = 0; i < tiposCartas.length; i++) {
			for (int j = 1; j <= 13; j++) {
				Carta carta = new Carta(j, tiposCartas[i]);
				mazo.add(carta);
			}
		}
	}
	public Carta sacaCarta() {
		Random random = new Random();
		int indice = random.nextInt(mazo.size()-1);
		return mazo.get(indice);
	}
}
